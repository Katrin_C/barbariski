@extends('layouts.admin')

@section('content')
    <h2>Users</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>name</th>
                <th>Full name</th>
                <th>email</th>
                <th>Role</th>
                @if(in_array(Auth::user()->type, [
                                                    App\User::TYPE_ADMIN,
                                                    App\User::TYPE_MANAGER]
                                                                        )))
                <th>Rate</th>
                @endif

            </tr>
            </thead>
            <tbody>
            @foreach( $users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->last_name}} {{$user->first_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role}}</td>
                    @if( in_array(Auth::user()->type, [
                                                        App\User::TYPE_ADMIN
                                                        ]
                                                                            )))
                    <td>{{$user->rate}} </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
