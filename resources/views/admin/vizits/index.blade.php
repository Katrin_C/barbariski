@extends('layouts.admin2')

@section('content')
    <h2>Vizits</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>parent</th>
                <th>child</th>
                <th>contact</th>
                <th>date</th>
                <th>notes parent</th>
                <th>notes vizit</th>
                <th>created_at</th>
                <th>vizit</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $vizits as $vizit)
                <tr>
                    <td>{{$vizit->id}}</td>
                    <td>{{$vizit->parent->last_name}} {{$vizit->parent->first_name}} ({{$vizit->parent->phone}})</td>
                    <td>{{$vizit->child->last_name}} {{$vizit->child->first_name}} (
                        {{$vizit->date->diffInYears($vizit->child->dob)}} г
                        {{$vizit->date->diffInMonths($vizit->child->dob) % 12}} мес
                        )
                    </td>
                    <td>{{$vizit->contact_last_name}} {{$vizit->contact_first_name}} ({{$vizit->contact_phone}})</td>
                    <td>{{$vizit->date->format('Y-m-d')}} {{$vizit->visit_time_from}} - {{$vizit->visit_time_to}}</td>
                    <td>{{$vizit->parent->notes}}</td>
                    <td>{{$vizit->notes}}</td>
                    <td>{{$vizit->created_at}}</td>
                    <td>
                            <button class="btn-success start" id='start-{{$vizit->id}}'  value="{{$vizit->start_time}}">
                                @if($vizit->start_time == null)
                                    Start
                                @else
                                    {{$vizit->start_time->format('h:m')}}
                                @endif
                            </button>


                        @if($vizit->end_time == null)
                            <button class="btn-success end" id='end-{{$vizit->id}}' value="{{$vizit->end_time}}"> End </button>
                        @endif
                        {{$vizit->start_time}} - {{$vizit->end_time}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!-- Display the countdown timer in an element -->
        <p id="demo"></p>


    </div>

@endsection

@section('jsforthispage')
    <script>
        $('.start').each(function() {
            var $this = $(this);
            console.log($this[0]['id']);
            console.log($this[0]['value']);

            // Set the date we're counting down to
            var startTime = $this[0]['value'];

            console.log(startTime);

            var countDownDate = new Date(startTime).getTime();

            console.log(countDownDate);

            // Update the count down every 1 second

            var x = setInterval(function() {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = now - countDownDate;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                document.getElementById("demo").innerHTML = hours + "h "
                    + minutes + "m " + seconds + "s ";

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("demo").innerHTML = "EXPIRED";
                }
            }, 1000);

        });

    </script>
@endsection
