@extends('layouts.admin')

@section('content')
    <h2>Clients</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>isMainBranch</th>
                <th>name</th>
                <th>phone</th>
                <th>email</th>
                <th>openTime - closeTime</th>
                <th>actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $filias as $filia)
                <tr>
                    <td>{{$filia->id}}</td>
                    <td>{{$filia->isMainBranch}}</td>
                    <td>{{$filia->name}}</td>
                    <td>{{$filia->phone}}</td>
                    <td>{{$filia->email}}</td>
                    <td>{{$filia->openTime}} - {{$filia->closeTime}}</td>
                    <td></td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
