@extends('layouts.admin')

@section('content')
    <h2>Clients</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">

            <header>
                <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                <h2>{{ trans('messages.add_client') }}</h2>
            </header>

           <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    {!! Form::open(['url' => 'admin/clients/' . $client->id, 'method' => 'PATCH', 'files' => true, 'class' => 'form-horizontal']) !!}

                    <fieldset>
                        <legend>{{ trans('messages.client_info') }}</legend>

                        <div class="row form-group {{ $errors->first('first_name') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.first_name') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('first_name', old('first_name', $client->first_name), ['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('first_name') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('last_name') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.last_name') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('last_name', old('last_name', $client->last_name), ['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('last_name') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('second_name') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.second_name') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('second_name', old('second_name', $client->second_name), ['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('second_name') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('gender') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.gender') }}</label>
                            </div>
                            <div class="col-md-9">

                                {!! Form::select('gender',
                                        ['F' => 'Female', 'M' => 'Male'],
                                        old('gender', $client->gender),
                                        ['class' => 'form-control'])
                                    !!}
                                <p class="bg-warning">{!! $errors->first('gender') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('dob') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.dob') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('dob', old('dob', $client->dob),['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('dob') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('phone') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.phone') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('phone', old('phone', $client->phone),['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('phone') !!}</p>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset>
                        <legend>{{ trans('messages.filia') }}</legend>
                        <div class="row form-group {{ $errors->first('filia') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.filia') }}</label>
                            </div>
                            <div class="col-md-9">

                                {!! Form::select('filia',
                                        ['C' => 'Club', 'G' => 'Garden'],
                                        old('filia', $client->filia),
                                        ['class' => 'form-control'])
                                    !!}
                                <p class="bg-warning">{!! $errors->first('filia') !!}</p>
                            </div>
                        </div>

                        <div class="row form-group {{ $errors->first('group') != '' ? ' has-error' : ''}}">
                            <div class="col-md-3">
                                <label class="control-label">{{ trans('messages.group') }}</label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('group', old('group', $client->group), ['class' => 'form-control']) !!}
                                <p class="bg-warning">{!! $errors->first('group') !!}</p>
                            </div>
                        </div>

                    </fieldset>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-save"></i>
                                    {{ trans('messages.update_changes') }}
                                </button>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </table>
    </div>
@endsection
