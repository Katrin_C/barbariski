@extends('layouts.admin')

@section('content')
    <h2>Clients</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>first_name - last_name  </th>
                <th>email</th>
                <th>phone</th>
                <th>children</th>
                <th>ballans</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->last_name}} {{$user->first_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>
{{--                        @foreach( $user->children as $index => $child)--}}
{{--                            @if($child->gender == 'female')--}}
{{--                                <i class="fa fa-female" style="color: hotpink"></i>--}}
{{--                            @else--}}
{{--                                <i class="fa fa-male" style="color: dodgerblue"></i>--}}
{{--                            @endif--}}

{{--                            {{$child->last_name}} {{$child->first_name}} ---}}
{{--                            {{\Carbon\Carbon::now('Europe/Kiev')->diffInYears($child->dob)}} г--}}
{{--                            {{\Carbon\Carbon::now('Europe/Kiev')->diffInMonths($child->dob) % 12}} мес--}}
{{--                            <br>--}}
{{--                        @endforeach--}}
                    </td>
                    <td>{{$user->ballans_hour}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
