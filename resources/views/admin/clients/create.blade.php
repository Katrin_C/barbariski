@extends('layouts.admin')

@section('content')
    <h2>{{ trans('messages.add_client') }}</h2>
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>{{ trans('messages.client_personal_info') }}</h2>
                                </header>


                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body">

                                        {!! Form::open(['url' => 'admin/clients', 'files' => true, 'class' => 'form-horizontal']) !!}

                                        {!! Form::hidden('manager_id', $manager->id) !!}

                                        <fieldset>

                                            <div class="row form-group {{ $errors->first('first_name') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.first_name') }}</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::text('first_name', old('first_name', ''), ['class' => 'form-control']) !!}
                                                    <p class="bg-warning">{!! $errors->first('first_name') !!}</p>
                                                </div>
                                            </div>

                                            <div class="row form-group {{ $errors->first('last_name') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.last_name') }}</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::text('last_name', old('last_name', ''), ['class' => 'form-control']) !!}
                                                    <p class="bg-warning">{!! $errors->first('last_name') !!}</p>
                                                </div>
                                            </div>

                                            <div class="row form-group {{ $errors->first('second_name') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.second_name') }}</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::text('second_name', old('second_name', ''), ['class' => 'form-control']) !!}
                                                    <p class="bg-warning">{!! $errors->first('second_name') !!}</p>
                                                </div>
                                            </div>

                                            <div class="row form-group {{ $errors->first('gender') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.gender') }}</label>
                                                </div>
                                                <div class="col-md-9">

                                                    {!! Form::select('gender',
                                                            ['F' => 'Female', 'M' => 'Male'],
                                                            old('gender', 'male'),
                                                            ['class' => 'form-control'])
                                                        !!}
                                                    <p class="bg-warning">{!! $errors->first('gender') !!}</p>
                                                </div>
                                            </div>

                                            <div class="row form-group {{ $errors->first('dob') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.dob') }}</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::text('dob', old('dob', ''),['class' => 'form-control']) !!}
                                                    <p class="bg-warning">{!! $errors->first('dob') !!}</p>
                                                </div>
                                            </div>

                                            <div class="row form-group {{ $errors->first('phone') != '' ? ' has-error' : ''}}">
                                                <div class="col-md-3">
                                                    <label class="control-label">{{ trans('messages.phone') }}</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::text('phone', old('phone', ''),['class' => 'form-control']) !!}
                                                    <p class="bg-warning">{!! $errors->first('phone') !!}</p>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">

                                <fieldset>
                                    <legend>{{ trans('messages.filia') }}</legend>
                                    <div class="row form-group {{ $errors->first('filia') != '' ? ' has-error' : ''}}">
                                        <div class="col-md-3">
                                            <label class="control-label">{{ trans('messages.filia') }}</label>
                                        </div>
                                        <div class="col-md-9">

                                            {!! Form::select('filia',
                                                    ['C' => 'Club', 'G' => 'Garden'],
                                                    old('filia', 'Garden'),
                                                    ['class' => 'form-control'])
                                                !!}
                                            <p class="bg-warning">{!! $errors->first('filia') !!}</p>
                                        </div>
                                    </div>

                                    <div class="row form-group {{ $errors->first('group') != '' ? ' has-error' : ''}}">
                                        <div class="col-md-3">
                                            <label class="control-label">{{ trans('messages.group') }}</label>
                                        </div>
                                        <div class="col-md-9">
                                            {!! Form::text('group', old('group', ''), ['class' => 'form-control']) !!}
                                            <p class="bg-warning">{!! $errors->first('group') !!}</p>
                                        </div>
                                    </div>

                                </fieldset>
                            </table>
                        </div>
                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                {{ trans('messages.save_changes') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
