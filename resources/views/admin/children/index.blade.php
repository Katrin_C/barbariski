@extends('layouts.admin2')

@section('content')
    <h2>Children</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>first_name - last_name</th>
                <th>gender</th>
                <th>ballans_hour</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $children as $child)
                <tr>
                    <td>{{$child->id}}</td>
                    <td>{{$child->last_name}} {{$child->first_name}}</td>
                    <td>
                        @if($child->gender == 'female')
                            <i class="fa fa-female" style="color: hotpink"></i>
                        @else
                            <i class="fa fa-male" style="color: dodgerblue"></i>
                        @endif
                        {{\Carbon\Carbon::now('Europe/Kiev')->diffInYears($child->dob)}} г
                        {{\Carbon\Carbon::now('Europe/Kiev')->diffInMonths($child->dob) % 12}} мес
                        <br>
                    </td>
                    <td>{{$child->ballans_hour}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
