@extends('layouts.admin2')

@section('content')
    <section class="page-section about-heading">
        <div class="container">
            {{--<img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="/design/img/2fd28a72e674dd2de3f1defe23efe7ad.png" alt="">--}}
            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">

                            <div class="panel-heading">{{Auth::user()->name}} Добро пожаловать!  </div>

                            <div class="panel-body">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <p>


                                </p>
                                    <a class="btn btn-primary btn-xl" href="{{url('proposals')}}">Темматические дни</a>
                                    <a class="btn btn-primary btn-xl" href="{{url('price')}}">Посмотреть стоимость</a>
                                    <a class="btn btn-primary btn-xl" href="{{url('createVisit')}}">Записаться на визит</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
