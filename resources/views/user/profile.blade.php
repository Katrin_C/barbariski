@extends('layouts.app')

@section('title')
    Стоимость услуг
@endsection

@section('content')
    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper"> {{Auth::user()->name}}, добро пожаловать!  </span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('includes.ribbon')

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <a class="btn btn-primary btn-xl" href="{{url('proposals')}}">Темматические дни</a>
                            <a class="btn btn-primary btn-xl" href="{{url('price')}}">Посмотреть стоимость</a>
                            <a class="btn btn-primary btn-xl" href="{{url('createVisit')}}">Записаться на визит</a>

                            <p class="mb-3">Персональные данные</p>
                            {!! Form::open([
                            'url' => "user/". Auth::user()->id,
                            'method' => 'PATCH',
                            'files' => true,
                            ]) !!}

                            {{--first_name & last_name--}}
                            <div class="input-group {{ $errors->first('first_name') != '' ? ' has-error' : ''}}
                            {{ $errors->first('last_name') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Фамилия</span>
                                <input id="last_name" type="text" class="form-control" name="last_name"
                                       placeholder="Фамилия"
                                       value="{{Auth::user()->last_name}}">

                                <span class="input-group-addon">Имя</span>
                                <input id="first_name" type="text" class="form-control" name="first_name"
                                       placeholder="Фамилия"
                                       value="{{Auth::user()->first_name}}">
                            </div>
                            <p class="bg-warning">{!! $errors->first('last_name') !!}</p>
                            <p class="bg-warning">{!! $errors->first('first_name') !!}</p>

                            {{--email--}}
                            <div class="input-group {{ $errors->first('email') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Email</span>
                                <input id="email" type="email" class="form-control" name="email" placeholder="Email"
                                       value="{{Auth::user()->email}}">
                            </div>
                            <p class="bg-warning">{!! $errors->first('email') !!}</p>

                            {{--phone--}}
                            <div class="input-group {{ $errors->first('phone') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Телефон</span>
                                <input id="phone" type="text" class="form-control" name="phone" placeholder="Телефон"
                                       value="{{Auth::user()->phone}}">
                            </div>
                            <p class="bg-warning">{!! $errors->first('phone') !!}</p>

                            {{--<br>--}}


                            {{--//ToDo--}}
                            <p style="display: none;">Обязательно к заполнению в случае заказа услуги "привести
                                малыша"</p>
                            {{--adress--}}
                            <div class="input-group" style="display: none;">
                                <span class="input-group-addon">Город</span>
                                <input id="city" type="text" class="form-control" name="city" placeholder="Город"
                                       value="{{Auth::user()->city}}">
                            </div>
                            <div class="input-group" style="display: none;">

                                <span class="input-group-addon">Улица</span>
                                <input id="street" type="text" class="form-control" name="street"
                                       placeholder="Улица"
                                       value="{{Auth::user()->street}}">

                                <span class="input-group-addon">Дом</span>
                                <input id="bilding" type="text" class="form-control" name="street" placeholder="Дом"
                                       value="{{Auth::user()->bilding}}">

                                <span class="input-group-addon">Кв.</span>
                                <input id="room" type="text" class="form-control" name="street" placeholder="Кв."
                                       value="{{Auth::user()->room}}">

                                <span class="input-group-addon">Подъезд</span>
                                <input id="entrance" type="text" class="form-control" name="street"
                                       placeholder="Подъезд"
                                       value="{{Auth::user()->entrance}}">
                            </div>
                            {{--<br>--}}

                            <div class="form-group {{ $errors->first('notes') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дополнительная информация</span>
                                <textarea class="form-control" rows="5" id="notes"
                                          name="notes">{{Auth::user()->notes}}</textarea>
                            </div>
                            <p class="bg-warning">{!! $errors->first('notes') !!}</p>

                            <button type="submit" class="btn btn-success pull-right" >Сохранить</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper"> Текущий балланс {{Auth::user()->ballans_hour}} : 00 часов</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(Auth::user()->children->count() > 0)

    <section class="page-section about-heading">
        <div class="container">
            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3"> Мои дети:

                            <!-- Trigger the modal with a button -->
                            <button type="button" class="btn btn-info pull-right"
                                    data-toggle="modal" data-target="#myModal"
                                    data-toggle="tooltip" data-placement="top"
                                    title="Зарегистрировать ребенка!">
                                <span class="fa fa-plus"></span>
                                <span class="fa fa-child"></span>
                                <span class="fa fa-child"></span>
                            </button>
                            </p>

                            <table class="table table-bordered">
                                <thead>
                                <th></th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>День рождения</th>
                                <th>Возраст</th>
                                <th>Примечания</th>
                                <th>Баланс часов</th>

                                </thead>
                                <tbody>
                                @foreach(Auth::user()->children as $child)
                                    <tr>
                                        <td> @if($child->gender == 'female')
                                                <i class="fa fa-female" style="color: hotpink"></i>
                                            @else
                                                <i class="fa fa-male" style="color: dodgerblue"></i>
                                            @endif

                                        </td>
                                        <td>{{$child->last_name}}</td>
                                        <td>{{$child->first_name}}</td>
                                        <td>{{$child->dob->format('d M')}}</td>
                                        <td>{{\Carbon\Carbon::now('Europe/Kiev')->diffInYears($child->dob)}} г
                                            {{\Carbon\Carbon::now('Europe/Kiev')->diffInMonths($child->dob) % 12}} мес
                                        </td>
                                        <td>{{$child->info}}</td>
                                        <td>{{$child->ballans_hour}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    <!-- Modal -->
    @include('user.addChild')
    @if(Auth::user()->vizits->count() > 0)
    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper"> Записи на визит</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">
            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">

                            <table class="table table-bordered">
                                <thead>
                                <th>Дата</th>
                                <th>Время</th>
                                <th>Ребенок</th>
                                <th>Примечания</th>
                                <th>Время пребывания</th>
                                <th>Длительность</th>

                                </thead>
                                <tbody>
                                @foreach(Auth::user()->vizits as $vizit)
                                    <tr>
                                        <td> @if($vizit->date > \Carbon\Carbon::now('Europe/Kiev'))
                                                 {{$vizit->date->format('d M Y')}}
                                            @else
                                                {{$vizit->date}}
                                            @endif
                                        </td>
                                        <td>{{$vizit->visit_time_from}} - {{$vizit->visit_time_to}}</td>
                                        <td>{{$vizit->child->last_name}} {{$vizit->child->first_name}}</td>
                                        <td>{{$vizit->notes}}</td>
                                        <td>{{$vizit->start_time}} - {{$vizit->end_time}}</td>
                                        <td>{{$vizit->duration}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endif



@endsection

@section('jsforthispage')
    <script type="text/javascript">

        $(document).on('click', '.btn-add-child', function (e) {
            var $this = $(this);

            var data = {};
            data.parrentId = "{{Auth::user()->id}}";
            data.last_name = document.getElementById('child_last_name').value;
            data.first_name = document.getElementById('child_first_name').value;
            data.gender = document.getElementById('child_gender').value;
            data.dob = document.getElementById('child_dob').value;
            data.info = document.getElementById('child_info').value;

            console.log(data);

            $.ajax({
                url: '{{ url('user') }}' + '/' + "{{Auth::user()->id}}" + '/addChild',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "put",
                data: data,
                success: function (response) {

                    console.log(response);

                    if (response.error) {
                        $.each(response.errorFields, function (index, item) {
                            console.log(index, item);
                            var $el = $('#error-child_' + index);
                            var errorText = '';
                            $.each(item, function (index2, item2) {
                                errorText += item2 + '</br>';
                            });
                            $el.html(errorText);
                        });

                        return;
                    }

                    $('#myModal').hide();
                    $("[data-dismiss=modal]").trigger({type: "click"});

                    location.reload();

                },
                error: function (response) {
                    console.log(response.status);
                }
            });
        });

    </script>

    <script type="text/javascript">

        $(function () {
            var today = new Date();

            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();

            var minAge = new Date(year - 1, month, day);


            console.log(today);
            console.log(minAge);

            $('#datetimepicker1').datetimepicker({
                locale: 'ru',
                viewMode: 'years',
                format: 'DD-MM-YYYY',
            });
        });
    </script>

@endsection
