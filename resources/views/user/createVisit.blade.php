@extends('layouts.app')

@section('title')
    Запись на визит
@endsection

@section('content')
    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">
                                    Текущий балланс {{Auth::user()->ballans_hour}} : 00 часов
                                <br>
                            </span>

                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('includes.ribbon')

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3">Для записи на занятие заполните форму</p>

                            {!! Form::open([
                            'url' => "createVisit",
                            'method' => 'PATCH',
                            'files' => true,
                            ]) !!}
                            <input id="id" type="text" class="form-control" name="id"
                                   value=@if(Auth::user()){{Auth::user()->id}} @else "newUser" @endif hidden="hidden">

                            <p class="mb-3">Укажите Ваши контактные данные ( или данные человека, который приведет малыша):</p>

                            {{--first_name & last_name--}}
                            <div class="input-group {{ $errors->first('first_name') != '' ? ' has-error' : ''}}
                            {{ $errors->first('last_name') != '' ? ' has-error' : ''}}">

                                <span class="input-group-addon">Фамилия</span>
                                {!! Form::text ('last_name', old('last_name', Auth::user()->last_name), [
                                                                'class' => 'form-control',
                                                                'id' => 'last_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Фамилия'
                                ])!!}

                                <span class="input-group-addon">Имя</span>
                                {!! Form::text ('first_name', old('first_name', Auth::user()->first_name), [
                                                                'class' => 'form-control',
                                                                'id' => 'first_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Имя'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('last_name') !!}</p>
                            <p class="bg-warning">{!! $errors->first('first_name') !!}</p>

                            {{--email--}}
                            <div class="input-group {{ $errors->first('email') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Email</span>
                                {!! Form::email ('email', old('email', Auth::user()->email), [
                                                                'class' => 'form-control',
                                                                'id' => 'email',
                                                                'required' => 'required',
                                                                'placeholder' => 'Email' , 'disabled'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('email') !!}</p>

                            {{--phone--}}
                            <div class="input-group {{ $errors->first('phone') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Телефон</span>
                                {!! Form::text ('phone', old('phone', Auth::user()->phone), [
                                                                'class' => 'form-control',
                                                                'id' => 'phone',
                                                                'required' => 'required',
                                                                'placeholder' => 'Телефон'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('phone') !!}</p>

                            <p class="mb-3">Данные ребенка</p>
                            <p class="mb-3">Мы предлагаем всем новым посетителям бесплатный первый визит.
                                <a href="{{url('price')}}">Подробнее.</a>
                            </p>


                            {{--child_gender--}}
                            <div class="input-group {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}
                            {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Укажите ребенка</span>
                                <select id="child_id" type="text" class="form-control" name="child_id"
                                        placeholder="Укажите ребенка" required="required">
                                    @foreach(Auth::user()->children as $child)
                                        <option value="{{$child->id}}">{{$child->last_name}} {{$child->first_name}}
                                            @if($child->ballans_hour > 0)
                                                - {{$child->ballans_hour}} часа бесплатно
                                            @endif
                                            @if(Auth::user()->ballans_hour > 0)
                                                - {{Auth::user()->ballans_hour}} абонемент
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon"></span>
                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info pull-right"
                                        data-toggle="modal" data-target="#myModal"
                                        data-toggle="tooltip" data-placement="top"
                                        title="Зарегистрировать ребенка!">
                                    <span class="fa fa-plus"></span>
                                    <span class="fa fa-child"></span>
                                    <span class="fa fa-child"></span>
                                </button>
                            </div>
                            <p class="bg-warning" id="error-child_gender"></p>

                            {{--visit time--}}
                            <div class="input-group {{ $errors->first('visit_date') != '' ? ' has-error' : ''}}
                            {{ $errors->first('visit_date') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дата Визита</span>
                                <div class='input-group date' id=''>
                                    <input type='text' class="form-control" id="visit_date" name="visit_date"  required="required"/>
                                    {{--<span class="input-group-addon">--}}
                                    {{--<span class="fa fa-calendar">--}}

                                    <span class="input-group-addon"> C </span>
                                    <input type='text' class="form-control" id="visit_time_from" name="visit_time_from"  required="required"/>
                                    <span class="input-group-addon"> До </span>
                                    <input type='text' class="form-control" id="visit_time_to" name="visit_time_to"  required="required"/>
                                    </span>
                                </div>
                            </div>
                            <p class="bg-warning" id="error-child_visit_time"></p>

                            <div class="form-group {{ $errors->first('notes') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дополнительная информация</span>
                                <textarea class="form-control" rows="5" id="notes"
                                          name="notes" value=@if(Auth::user())@endif ></textarea>
                            </div>
                            <p class="bg-warning">{!! $errors->first('notes') !!}</p>

                            <button type="submit" class="btn btn-success pull-right" >Сохранить</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    @include('user.addChild')

@endsection

@section('jsforthispage')

    <script type="text/javascript">

        $('#child_dob_datetimepicker').datetimepicker({
            locale: 'ru',
            viewMode: 'months',
            minDate: moment().locale('ru').subtract(8,'y'),
            maxDate: moment().locale('ru').subtract(1,'y'),
            format: 'DD-MM-YYYY',
        });

        $('#visit_date').datetimepicker({
            locale: 'ru',
            format: 'DD-MM-YYYY',
            minDate: moment('2018-03-06').locale('ru'),
            daysOfWeekDisabled: [0],
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }

        });

        $('#datetimepicker1').datetimepicker({
            locale: 'ru',
            viewMode: 'years',
            minDate: moment().locale('ru').subtract(8,'y'),
            maxDate: moment().locale('ru').subtract(1,'y'),
            format: 'DD-MM-YYYY',
        });

        var minTime = new Date();

        var maxTime = new Date();

        $('#visit_time_from').datetimepicker({
            locale: 'ru',
            format: 'HH:mm',
            minDate: minTime.setHours(08,59),
            maxDate: maxTime.setHours(20,00),
            stepping: 30,
        });

        $('#visit_time_to').datetimepicker({
            locale: 'ru',
            format: 'HH:mm',
            maxDate: maxTime.setHours(10,00),
            maxDate: maxTime.setHours(22,00),
        });
        $("#visit_time_from").on("dp.change", function (e) {
            $('#visit_time_to').data("DateTimePicker").minDate(e.date.add(1, 'hour'));
        });

    </script>

    <script type="text/javascript">

        $(document).on('click', '.btn-add-child', function (e) {
            var $this = $(this);

            var data = {};
            data.parrentId = "{{Auth::user()->id}}";
            data.last_name = document.getElementById('child_last_name').value;
            data.first_name = document.getElementById('child_first_name').value;
            data.gender = document.getElementById('child_gender').value;
            data.dob = document.getElementById('child_dob').value;
            data.info = document.getElementById('child_info').value;

            console.log(data);

            $.ajax({
                url: '{{ url('user') }}' + '/' + "{{Auth::user()->id}}" + '/addChild',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "put",
                data: data,
                success: function (response) {

                    console.log(response);

                    if (response.error) {
                        $.each(response.errorFields, function (index, item) {
                            console.log(index, item);
                            var $el = $('#error-child_' + index);
                            var errorText = '';
                            $.each(item, function (index2, item2) {
                                errorText += item2 + '</br>';
                            });
                            $el.html(errorText);
                        });
                        return;
                    }

                    $('#myModal').hide();
                    $("[data-dismiss=modal]").trigger({type: "click"});

                    location.reload();

                },
                error: function (response) {
                    console.log(response.status);
                }
            });
        });

    </script>



@endsection
