<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Данные о ребенке</h4>
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            </div>
            <div class="modal-body">

                {!! Form::open() !!}

                {{--child_first_name & child_last_name--}}
                <div class="input-group {{ $errors->first('child_first_name') != '' ? ' has-error' : ''}}
                {{ $errors->first('child_last_name') != '' ? ' has-error' : ''}}">
                    <span class="input-group-addon">Фамилия</span>
                    {!! Form::text ('child_last_name', old('child_last_name', Auth::user()->last_name), [
                    'class' => 'form-control',
                    'id' => 'child_last_name',
                    'required' => 'required',
                    'placeholder' => 'Фамилия'
                    ])!!}


                    <span class="input-group-addon">Имя</span>
                    <input id="child_first_name" type="text" class="form-control" name="child_first_name"
                           placeholder="Имя">
                </div>
                <p class="bg-warning" id="error-child_last_name"></p>
                <p class="bg-warning" id="error-child_first_name"></p>

                {{--child_dob--}}
                <div class="input-group {{ $errors->first('child_dob') != '' ? ' has-error' : ''}}
                {{ $errors->first('child_dob') != '' ? ' has-error' : ''}}">
                    <span class="input-group-addon">Дата рождения</span>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" id="child_dob" name="child_dob"/>
                        <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                    </div>
                </div>
                <p class="bg-warning" id="error-child_dob"></p>

                {{--child_gender--}}
                <div class="input-group {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}
                {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}">
                    <span class="input-group-addon">Пол ребенка</span>
                    <select id="child_gender" type="text" class="form-control" name="child_gender"
                            placeholder="Пол ребенка">
                        <option value="male">Мальчик</option>
                        <option value="female">Девочка</option>

                    </select>
                </div>
                <p class="bg-warning" id="error-child_gender"></p>

                {{--child_info--}}
                <div class="form-group {{ $errors->first('child_info') != '' ? ' has-error' : ''}}">
                    <span class="input-group-addon">Дополнительная информация</span>
                    <textarea class="form-control" rows="5" id="child_info"
                              name="child_info">{{Auth::user()->notes}}</textarea>
                </div>
                <p class="bg-warning" id="error-child_info"></p>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-success btn-add-child" id="add_child">Сохранить</button>
            </div>
        </div>

    </div>
</div>