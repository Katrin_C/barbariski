@extends('layouts.app')

@section('title')
    Стоимость услуг
@endsection

@section('content')
    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">

                            <span class="section-heading-upper">
                                @if(Auth::user())
                                    Текущий балланс {{Auth::user()->ballans_hour}} : 00 часов
                                    @else
                                    Если у Вас уже есть аккаунт на нашем сайте, то ввойдите в кабинет.
                                    @endif
                            </span>

                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('includes.ribbon')

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3">Для записи на занятие заполните форму</p>
                            <p class="mb-3">Мы предлагаем всем новым посетителям бесплатный первый визит. <a href="{{url('price')}}">Подробнее.</a></p>

                            {!! Form::open([
                            'url' => "createVisit",
                            'method' => 'PATCH',
                            'files' => true,
                            ]) !!}
                            <input id="id" type="text" class="form-control" name="id"
                                   value=@if(Auth::user()){{Auth::user()->id}} @else "newUser" @endif hidden="hidden">

                            {{--first_name & last_name--}}
                            <div class="input-group {{ $errors->first('first_name') != '' ? ' has-error' : ''}}
                            {{ $errors->first('last_name') != '' ? ' has-error' : ''}}">

                                <span class="input-group-addon">Фамилия</span>
                                {!! Form::text ('last_name', old('last_name'), [
                                                                'class' => 'form-control',
                                                                'id' => 'last_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Фамилия'
                                ])!!}

                                <span class="input-group-addon">Имя</span>
                                {!! Form::text ('first_name', old('first_name'), [
                                                                'class' => 'form-control',
                                                                'id' => 'first_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Имя'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('last_name') !!}</p>
                            <p class="bg-warning">{!! $errors->first('first_name') !!}</p>

                            {{--email--}}
                            <div class="input-group {{ $errors->first('email') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Email</span>
                                {!! Form::email ('email', old('email'), [
                                                                'class' => 'form-control',
                                                                'id' => 'email',
                                                                'required' => 'required',
                                                                'placeholder' => 'Email'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('email') !!}</p>

                            {{--phone--}}
                            <div class="input-group {{ $errors->first('phone') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Телефон</span>
                                {!! Form::text ('phone', old('phone'), [
                                                                'class' => 'form-control',
                                                                'id' => 'phone',
                                                                'required' => 'required',
                                                                'placeholder' => 'Телефон'
                                ])!!}
                            </div>
                            <p class="bg-warning">{!! $errors->first('phone') !!}</p>

                            <p class="mb-3">Данные ребенка</p>

                            {{--child_first_name & child_last_name--}}
                            <div class="input-group {{ $errors->first('child_first_name') != '' ? ' has-error' : ''}}
                            {{ $errors->first('child_last_name') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Фамилия</span>
                                {!! Form::text ('child_last_name', old('child_last_name'), [
                                                                'class' => 'form-control',
                                                                'id' => 'child_last_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Фамилия Ребенка'
                                ])!!}
                                <span class="input-group-addon">Имя</span>
                                {!! Form::text ('child_first_name', old('child_first_name'), [
                                                                'class' => 'form-control',
                                                                'id' => 'child_first_name',
                                                                'required' => 'required',
                                                                'placeholder' => 'Имя Ребенка'
                                ])!!}
                            </div>
                            <p class="bg-warning" id="error-child_last_name"></p>
                            <p class="bg-warning" id="error-child_first_name"></p>

                            {{--child_dob--}}
                            <div class="input-group {{ $errors->first('child_dob') != '' ? ' has-error' : ''}}
                            {{ $errors->first('child_dob') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дата рождения</span>
                                <div class='input-group date' id='child_dob_datetimepicker1'>
                                    <input type='text' class="form-control" id="child_dob_datetimepicker"
                                           name="child_dob" required="required"/>
                                    <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                                </div>
                            </div>
                            <p class="bg-warning" id="error-child_dob"></p>

                            {{--child_gender--}}
                            <div class="input-group {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}
                            {{ $errors->first('child_gender') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Пол ребенка</span>
                                <select id="child_gender" type="text" class="form-control" name="child_gender"
                                        placeholder="Пол ребенка" required="required">
                                    <option value="male">Мальчик</option>
                                    <option value="female">Девочка</option>

                                </select>
                            </div>
                            <p class="bg-warning" id="error-child_gender"></p>

                            {{--visit time--}}
                            <div class="input-group {{ $errors->first('visit_time') != '' ? ' has-error' : ''}}
                            {{ $errors->first('visit_time') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дата Визита</span>
                                <div class='input-group date' id=''>
                                    <input type='text' class="form-control" id="visit_date" name="visit_time"  required="required"/>
                                    {{--<span class="input-group-addon">--}}
                                    {{--<span class="fa fa-calendar">--}}
                                    <span class="input-group-addon"> C </span>
                                    {!! Form::select('visit_time_from',
                                            [
                                                '09:00'=> '09:00','09:30'=> '09:30',
                                                '10:00'=> '10:00','10:30'=> '10:30',
                                                '11:00'=> '11:00','11:30'=> '11:30',
                                                '12:00'=> '12:00','12:30'=> '12:30',
                                                '13:00'=> '13:00','13:30'=> '13:30',
                                                '14:00'=> '14:00','14:30'=> '14:30',
                                                '15:00'=> '15:00','15:30'=> '15:30',
                                                '16:00'=> '16:00','16:30'=> '16:30',
                                                '17:00'=> '17:00','17:30'=> '17:30',
                                                '18:00'=> '18:00','18:30'=> '18:30',
                                                '19:00'=> '19:00','19:30'=> '19:30',
                                                '20:00'=> '20:00',
                                            ],
                                            old('visit_time_from'),['class' => 'form-control', 'required' => 'required',]) !!}
                                    <span class="input-group-addon"> До </span>
                                    {!! Form::select('visit_time_to',
                                            [
                                                '10:00'=> '10:00','10:30'=> '10:30',
                                                '11:00'=> '11:00','11:30'=> '11:30',
                                                '12:00'=> '12:00','12:30'=> '12:30',
                                                '13:00'=> '13:00','13:30'=> '13:30',
                                                '14:00'=> '14:00','14:30'=> '14:30',
                                                '15:00'=> '15:00','15:30'=> '15:30',
                                                '16:00'=> '16:00','16:30'=> '16:30',
                                                '17:00'=> '17:00','17:30'=> '17:30',
                                                '18:00'=> '18:00','18:30'=> '18:30',
                                                '19:00'=> '19:00','19:30'=> '19:30',
                                                '20:00'=> '20:00','20:30'=> '20:30',
                                                '21:00'=> '21:00'
                                            ],
                                            old('visit_time_from'),['class' => 'form-control','required' => 'required',]) !!}
                                    </span>
                                </div>
                            </div>
                            <p class="bg-warning" id="error-child_visit_time"></p>

                            <div class="form-group {{ $errors->first('notes') != '' ? ' has-error' : ''}}">
                                <span class="input-group-addon">Дополнительная информация</span>
                                <textarea class="form-control" rows="5" id="notes"
                                          name="notes" value=@if(Auth::user())@endif ></textarea>
                            </div>
                            <p class="bg-warning">{!! $errors->first('notes') !!}</p>

                            <button type="submit" class="btn btn-success pull-right" >Сохранить</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsforthispage')

    <script type="text/javascript">

        $('#child_dob_datetimepicker').datetimepicker({
            locale: 'ru',
            viewMode: 'months',
            minDate: moment().locale('ru').subtract(8,'y'),
            maxDate: moment().locale('ru').subtract(1,'y'),
            format: 'DD-MMMM-YYYY',
        });

        $('#visit_date').datetimepicker({
            locale: 'ru',
            format: 'DD-MMMM-YYYY',
            minDate: moment('2018-03-06').locale('ru'),
            daysOfWeekDisabled: [0],
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }

        });


    </script>

@endsection
