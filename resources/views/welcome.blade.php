@extends('layouts.app')

@section('title')
    Новости
@endsection


@section('content')

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            {{--<span class="section-heading-upper">Our Promise</span>--}}
                            <span class="section-heading-lower">Мы придем на помощь,</span>
                        </h2>
                        <p class="mb-0">Когда на улице плохая погода, а ребенок просит развлечений
                            <br>много неотложных дел и вам не с кем оставить малыша,
                            <br>или нужно поработать на компьютере, а ребенок хочет поиграть
                            <br>а может Вы просто давно не были в кино или театре
                            <br>или вы хотите сделать беби-пати, а нет ни сил, ни желания убирать весь дом после детской вечеринки,
                            <br>то Вам нужно обязательно обратиться к нам!
                            <br><br>Мы займем малыша, если вам нужно отдохнуть или поработать.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">
            {{--<img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="/design/img/2fd28a72e674dd2de3f1defe23efe7ad.png" alt="">--}}
            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3" style="font-weight: 800">
                                ЧТО ИМЕННО МЫ ПРЕДЛАГАЕМ:
                            </p>
                            <p class="mb-3"style="font-weight: 800">
                                НЯНИ В УДОБНОЕ ВРЕМЯ ИМЕННО ДЛЯ ВАС
                            </p>
                            <p>
                                Двери нашего клуба открыты с 9:00 до 21:00 для всех маленьких гостей.
                                А в летнее время мы планируем его увеличить !
                                Наш сервис поможет Вам распределить свое время с пользой и для Вас, и для малыша!
                            <p class="mb-3"style="font-weight: 800">
                                ДЛЯ МАЛЫШЕЙ ОТ 1 ГОДА ДО 7 ЛЕТ
                            </p>
                            <p>
                                Игровая зона нашего клуба будет интересна как для крошек от 1 года так и для малышей
                                постарше. Наши няни найдут занятие для любого возраста.
                                <br>
                                Мы проведем время с радостью и с пользой, играя с другими детьми в увлекательные игры.
                            </p>
                            <p class="mb-3"style="font-weight: 800">
                                НАША ЦЕЛЬ - ВЕСЕЛОЕ РАЗВИТИЕ ВАШЕГО МАЛЫША
                            </p>
                            <p>
                                У нас нет специальных программ развития.
                                <br>У нас нет графика и расписания занятий.
                                <br>У нас нет прогулов и отставаний...
                                <br>Зато у нас есть активное время и веселый подход.
                                <br>У нас свободное времяпрепровождение в клубе, которое дети с удовольствием чередуют
                                с творческими занятиями (росписи фигурок, рисование, раскраски, нанизывание бус и многое другое).
                                <br><b>Главное - это ДЕТСТВО и развиваться мы будем через игру.</b>
                                <br>
                            </p>
                            <p class="mb-3"style="font-weight: 800">
                                ДЕТИ ПОД НАБЛЮДЕНИЕМ
                            </p>
                            <p>
                                На территории клуба работает online камера для присмотра за вашими малышами.
                                <br>В то время, когда Вы занимаетесь ежедневными хлопотами, мы организуем увлекательное
                                времяпрепровождение для вашего ребенка, под присмотром и в безопасности.

                            </p>
                            <p class="mb-3"style="font-weight: 800">
                                ПРИЯТНАЯ ЦЕНА
                            </p>
                            <p>
                                Гибкая ценовая политика поможет Вам выбрать удобный вариант оплаты. Можно заглянуть к
                                нам на часок, а можно и надолго подружиться. А что может быть лучше? Тем более, если
                                есть классные <a href="{{url('price')}}"> абонементы</a> <i class="fa fa-smile-o"></i>
                            </p>
                            <a class="btn btn-primary btn-xl" href="{{url('howitworks')}}">Записаться на визит</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-lower">Приходите к нам поиграть !</span>
                        </h2>
                        <p class="mb-0">В Вашем распоряжении уютная игровая комната с множеством игрушек и занимательных игр,
                            услуги временного присмотра за детьми,
                            возможность аренды клуба для проведения детского Дня рождения!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection