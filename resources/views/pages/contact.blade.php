@extends('layouts.app')

@section('title')
    контакты
@endsection

@section('content')

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-5">
                            <span class="section-heading-lower">Ждем вас</span>
                            <span class="section-heading-upper">Часы работы: *</span>
                        </h2>
                        <ul class="list-unstyled list-hours mb-5 text-left mx-auto">
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Понедельник
                                <span class="ml-auto">9:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Вторник
                                <span class="ml-auto">9:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Среда
                                <span class="ml-auto">9:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Четверг
                                <span class="ml-auto">9:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Пятница
                                <span class="ml-auto">9:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Суббота
                                <span class="ml-auto">11:00 - 21:00</span>
                            </li>
                            <li class="list-unstyled-item list-hours-item d-flex">
                                Воскресенье
                                <span class="ml-auto">Выходной *</span>
                            </li>
                            <p class="mb-0">
                                <small>
                                    <em>* В летнее время в расписании возможны изменения</em>
                                </small>
                            </p>
                        </ul>
                        <p class="address mb-5">
                            <em>
                                <strong>ЖК 21 Жемчужина</strong>
                                <br>
                                Одесса, ул. Жемчужная 4
                            </em>
                        </p>
                        <p class="mb-0">
                            <small>
                                <em>Звоните </em>+38 (068) 183 28 48<em> Viber</em>
                            </small>
                        <br>
                            <small>
                                <em>Пишите </em>
                                <a href="mailto:barbariski.club@gmail.com?Subject=Hello"target="_top">barbariski.club@gmail.com</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Map -->
    <section id="contact" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2752.3947945509!2d30.710411408734057!3d46.38139428226793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c7cb5658ffd10f%3A0x725ec60f6d5bf0ee!2z0JHQsNGA0LHQsNGA0LjRgdC60LgsINC90Y_QvdGPLdC60LvRg9Cx!5e0!3m2!1sen!2sua!4v1518008692269"
                width="100%" height="450px" frameborder="0"
                scrolling="no" marginheight="0" marginwidth="0">
        </iframe>
    </section>

@endsection