@extends('layouts.app')

@section('title')
    Галерея
@endsection

@section('content')

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            {{--<span class="section-heading-upper">Our Promise</span>--}}
                            <span class="section-heading-lower">Наши улыбки</span>
                        </h2>
                        <p class="mb-0"> Страница на реконструкции</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection