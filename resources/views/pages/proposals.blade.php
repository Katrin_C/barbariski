@extends('layouts.app')

@section('title')
    Наши предложения
@endsection

@section('content')

    <section class="page-section cta">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="cta-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            {{--<span class="section-heading-upper">Our Promise</span>--}}
                            <span class="section-heading-lower">Наши предложения</span>
                        </h2>
                        <p class="mb-0"> Страница постоянно обновляется</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Темматические дни(часы)</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3">Мы постоянно проводим творческие занятия с ребятами разных возрастов.
                                <br>Играем, лепим, вырезаем, клеем, рисуем. (Как правило каждый малыш после визита приносит что-то домой)</p>
                            <p>
                                <em> Пожелания на март 2018</em>
                                <br> <b>6 марта</b> - День цветов
                                <br> <b>10 марта</b> - Весенний цветок
                                <br> <b>14 марта</b> - Весенний лес
                                <br> <b>21 марта</b> - Перелетные птички
                                <br> <b>23 марта</b> - Солнечный зайчик
                                <br> <b>29 марта</b> - Верба. Вербное воскресение
                                <br>
                                <br><em> Пожелания на апрель 2018</em>
                                <br> <b>1 апреля</b> - День Смеха
                                <br> <b>2 апреля - 4 апреля </b> -  Пасхальное настроение
                                <br> <b>12 апреля</b> -  Космонавтика
                                <br> <em> Если есть пожелания пишите нам</em>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Празднуйте дни рождения у нас</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3"></p>
                            <p>
                                Мы рады предоставить помещение для празднования детского Дня рождения.
                                <br>Мы не навязываем свои анимационные программы - мы предоставляем просторное помещение с игровой зонной для вашего отдыха.
                                На время Вашего пребывания других деток в клубе нет и наша няня полностью в Вашем распоряжении.
                                <br>Подробности по телефону:
                                <br>+38 068 183 28 48
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection