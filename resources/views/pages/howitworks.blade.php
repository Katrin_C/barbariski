@extends('layouts.app')


@section('title')
    Как это работает?
@endsection

@section('content')

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Как это работает?</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3">КОГДА МОЖНО ЗНАКОМИТЬ МАЛЫША С БАРБАРИСКАМИ?</p>
                            <p>Когда готова мама и, конечно, если малышу больше 1 года и он уверенно ходит и стоит на ногах!
                                <br>
                                *для комфортного первого раза просим Вас предварительно записаться на визит.
                            </p>
                            <p class="mb-3">МОЖНО ЛИ ОСТАВИТЬ МАЛЫША ПОД ПРИСМОТРОМ БАРБАРИСОК?</p>
                            <p>
                                Конечно!
                                НЯНЯ КЛУБ специально оборудован и подготовлен для приема маленьких гостей с 9:00 до
                                21:00 по адресу ул. Жемчужная 4!
                            </p>
                            <p class="mb-3">СКОЛЬКО ВРЕМЕНИ МАЛЫШ МОЖЕТ БЫТЬ В КЛУБЕ?</p>
                            <p>От 1 часа до 4 часов необходимого времени маме!
                                <br>
                                *если малыш находится у нас дольше 2 часов БАРБАРИСКИ просят давать с собой ланч-бокc
                                (только вода, сок в упаковке, сухие печенья или галеты)
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">А чем НЯНЯ занимается с малышом?</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p>Каждое пребывание малыша у нас это веселое и полезное время.
                                Вне зависимости от того в какое время вы пришли и как на долго – мы приготовили для него занимательное времяпрепровождения:</p>
                            <ul>
                                <li>Занимательную активную игру;</li>
                                <li>Раскраску, карандаши;</li>
                                <li>Краски, пластилин и многое другое;</li>
                                <li>Ворк-шоп сюрприз;</li>
                                <li>Множество игрушек и спортивный комплекс</li>
                                <li>Много веселья!!</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection