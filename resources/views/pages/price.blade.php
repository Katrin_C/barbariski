@extends('layouts.app')

@section('title')
    Стоимость услуг
@endsection

@section('content')
    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">ТАРИФЫ</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3">Разовое посещение</p>
                            <p>Если Вам необходимо отлучиться или просто заняться своими делами, а дедушки\бабушки устали или просто далеко,
                                приводите Вашего малыша к нам.
                                <br>Мы постараемся cделать так, чтобы ему было весело,а Вам не пришлось за него беспокоиться.
                                <br> <em>1 час - 50грн</em>
                                <br> <em>2 часа - 90грн</em>
                                <br> <em>3 часа - 120грн</em>
                                <br> <em>4 часа - 150грн</em>
                                <br>
                                Если Вы планируете прийти к нам впервые и вашему малышу нет 3-х лет, мы рекомендуем визит длительностью не более 2-х часов.
                                Для комфортного привыкания, пожалуйста, запишитесь заранее на визит.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Абонементы</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3"></p>
                            <p>Если вам необходима регулярная помощь по присмотру за вашим ребенком или их у вас несколько,
                                мы можем предложить приобрести абонемент на:
                                <br> <em>20 часов - 900грн</em>
                                <br> <em>40 часов - 1600грн</em>
                                <br> <em>80 часов - 2800грн</em>
                                <br> Абонемент действителен в течение месяца с момента приобретения, а часы Вы используйте как вам удобно.
                                <br> <b>Отличная экономия!</b> Однако, нужно учесть, что оставшиеся часы «не замораживаются» и в случае неиспользования «сгорают».
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pc">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <div class="pc-inner text-center rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Акции и специальные условия</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section about-heading">
        <div class="container">

            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto">
                        <div class="bg-faded rounded p-5">
                            <p class="mb-3"> Первый визит бесплатно</p>
                            <p>Запишись на свой первый визит и приходи поиграть к нам абсолютно бесплатно.
                                <br>
                                <em>По условиям данного предложения мы ждем Ваших малышей на первый визит только по электронной регистрации
                                    или предварительной записи у нашего администратора. Мы хотим быть уверенными, что наши няни смогут уделить
                                    достаточно времени новому малышу и Вы останетесь довольны от посещения БАРБАРИСОК.
                                    Мы предлагаем Вам 2 часа свободного времени.</em>
                            </p>
                            <p class="mb-3"> Абонемент на 3 месяца</p>
                            <p>Только в марте месяце при покупке любого абонемента срок его действия будет составлять 90дней.
                                <br>
                                <em>Это отличная возможность для наших постоянных посетителей или родителей нескольких деток</em>
                            </p>
                            <p class="mb-3"> День именинника</p>
                            <p>Приходи к нам в свой День Рождения.
                                <br>
                                <em>БАРБАРИСКИ приглашают провести свой день рождение вмести с нами.
                                    Вы можете арендовать наш клуб для своих гостей или просто прийти к нам весело провести время.
                                    В качестве подарка визит для именинника бесплатно.</em>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection