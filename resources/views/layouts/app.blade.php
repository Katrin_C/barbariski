<!DOCTYPE html>
<html lang="en">

<head>

    @include('includes.head')

</head>

<body>
<div class="flex-center position-ref full-height">
@if (Route::has('login'))
    <div class="top-right links">
        @auth
            <a href="{{ url('/user/').'/'.Auth::user()->id}}">Кабинет</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
                @endauth
    </div>
</div>

@endif

<h1 class="site-heading text-center text-white d-none d-lg-block">
    <span class="site-heading-upper mb-3">няня клуб</span>
    <span class="site-heading-lower">Барбариски</span>
    <span class="site-heading-upper mb-3 text-primary ">Клуб для мам, которые успеют все!   </span>
</h1>

<!-- Navigation -->
@include('includes.navbar')

<div class="flex-center position-ref full-height">


    @yield('content')

</div>


<footer class="footer text-faded text-center py-5">
    <div class="container">
        <p class="m-0 small">Copyright &copy; BARBARISKI 2018</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/design/vendor/jquery/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="/design/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="/bower_components/moment/min/moment-with-locales.js"></script>
<script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

@yield('jsforthispage')

</body>

</html>

