<!-- FLASH MESSAGES -->
@if(Session::has('error'))
    <div class="alert alert-danger fade in" style="opacity: 100;">
        <button class="close" data-dismiss="alert">
            ×
        </button>
        <i class="fa-fw fa fa-times"></i>
        <strong>Error!</strong> {{ Session::get('error') }}
    </div>
@endif

@if(Session::has('warning'))
    <div class="alert alert-warning fade in" style="opacity: 100;">
        <button class="close" data-dismiss="alert">
            ×
        </button>
        <i class="fa-fw fa fa-warning"></i>
        <strong>Warning!</strong> {{ Session::get('warning') }}
    </div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success fade in" style="opacity: 100;">
        <button class="close" data-dismiss="alert">
            ×
        </button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Success!</strong> {{ Session::get('success') }}
    </div>
@endif
