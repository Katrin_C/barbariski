<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active px-lg-4 dropdown">
                    <a href="/" class="nav-link text-uppercase text-expanded dropdown-toggle "data-toggle="dropdown" >
                        О НАС <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/" class="nav-link text-uppercase text-expanded "> Давайте знакомиться! </a></li>
                        <li><a href="/howitworks" class="nav-link text-uppercase text-expanded "> Как это работает? </a></li>
                        <li><a href="/proposals" class="nav-link text-uppercase text-expanded "> Наши предложения </a></li>
                        <li><a href="/rules" class="nav-link text-uppercase text-expanded "> Правила клуба </a></li>
                    </ul>
                </li>

                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/photos">Галерея</a>
                </li>
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/news">Новости</a>
                </li>
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/contact">КОНТАКТЫ</a>
                </li>
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded " href="/price" > Стоимость услуг </a>
                </li>
            </ul>
        </div>
    </div>
</nav>