<?php

return [

    'add_client' => 'Add client',
    'client_personal_info' => 'Client personal info',
    'save_changes' => 'Save',
    'client_info' => 'Client info',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'second_name' => 'Second name',
    'gender' => 'gender',
    'dob' => 'birthday',
    'phone' => 'phone',
    'filia' => 'filia and group',
    'group' => 'group',
    'update_changes' => 'Update'


];