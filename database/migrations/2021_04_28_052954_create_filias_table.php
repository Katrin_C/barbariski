<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiliasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('openTime')->nullable();
            $table->string('closeTime')->nullable();
            $table->string('email')->nullable();
            $table->tinyInteger('isMainBranch')->default(0);

            $table->integer('city')->nullable();
            $table->text('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filias');
    }
}
