<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsPaymentsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('gender')->after('ballans_hour')->nullable();
            $table->dateTime('dob')->after('gender')->nullable();
            $table->string('role')->after('dob')->default(\App\User::TYPE_USER)->index();
            $table->integer('rate')->after('role')->nullable();
            $table->integer('balance')->after('rate')->nullable();
            $table->dateTime('firstDay')->after('balance')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('dob');
            $table->dropColumn('role');
            $table->dropColumn('rate');
            $table->dropColumn('balance');
            $table->dropColumn('firstDay');
        });
    }
}
