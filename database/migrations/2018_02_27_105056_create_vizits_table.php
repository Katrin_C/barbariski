<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVizitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vizits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('contact_first_name');
            $table->string('contact_last_name');
            $table->string('contact_phone');
            $table->integer('child_id');
            $table->dateTime('date');
            $table->string('visit_time_from');
            $table->string('visit_time_to');
            $table->text('notes')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->string('duration')->nullable();
            $table->text('admin_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vizits');
    }
}
