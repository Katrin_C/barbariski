<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserDataToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->text('notes')->nullable()->after('type');
            $table->integer('floor')->nullable()->after('type');
            $table->integer('entrance')->nullable()->after('type');
            $table->integer('room')->nullable()->after('type');
            $table->integer('bilding')->nullable()->after('type');
            $table->string('street')->nullable()->after('type');
            $table->string('city')->nullable()->after('type');
            $table->integer('ballans_hour')->default(0)->after('type');

            $table->string('last_name')->nullable()->after('type');
            $table->string('first_name')->nullable()->after('type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->dropColumn('floor');
            $table->dropColumn('entrance');
            $table->dropColumn('room');
            $table->dropColumn('bilding');
            $table->dropColumn('street');
            $table->dropColumn('city');
            $table->dropColumn('ballans_hour');
            $table->dropColumn('last_name');
            $table->dropColumn('first_name');
        });
    }
}
