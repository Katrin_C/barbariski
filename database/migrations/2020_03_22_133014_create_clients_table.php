<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('gender');
            $table->dateTime('dob')->nullable();
            $table->string('phone')->nullable();

            $table->string('filia')->nullable();
            $table->string('group')->nullable();

            $table->dateTime('first_vizit')->nullable();
            $table->integer('manager_id')->nullable();
            $table->text('info')->nullable();
            $table->tinyInteger('is_garden_client')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
