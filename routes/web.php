<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');


Route::get('user/{id}', 'ProfileController@showProfile');
Route::resource('user', 'ProfileController');
Route::put('user/{id}/addChild', 'ProfileController@addChild');
Route::get('createVisit', 'ProfileController@createVisit');
Route::PATCH('createVisit', 'ProfileController@postVisit');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/proposals', 'PageController@proposals')->name('proposals');
Route::get('/rules', 'PageController@rules')->name('rules');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/news', 'PageController@news')->name('news');
Route::get('/photos', 'PageController@photos')->name('photos');
Route::get('/price', 'PageController@price')->name('price');
Route::get('/howitworks', 'PageController@howitworks')->name('howitworks');


Route::group([
    'middleware' => 'auth',
],
    function () {
        Route::group([
            'middleware' => 'admin',
            'prefix' => 'admin',
            'namespace' => 'Admin',
        ], function () {
            Route::get('/', 'HomeController@index');
            Route::get('home', 'HomeController@index');
            Route::resource('users', 'UserController');
            Route::resource('clients', 'ClientController');
            Route::resource('filias', 'FiliaController');
            Route::get('visits/{type}', 'VizitController@index');
            Route::resource('visits', 'VizitController');
//            Route::resource('children', 'ChildController');
            Route::get('dashboard', 'HomeController@dashboard');
        });

    });
