<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const TYPE_TEACHER = 'teacher';
    const TYPE_ADMIN = 'admin';
    const TYPE_MANAGER = 'manager';

    protected $casts = [
        'is_active' => 'boolean',
    ];
}
