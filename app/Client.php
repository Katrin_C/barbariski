<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'dob',
        'first_vizit'
    ];

    protected $casts = [
        'is_garden_client' => 'boolean',
    ];



}
