<?php

namespace App\Http\Controllers\Admin;

use App\Vizit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VizitController extends AdminController
{
    public function index($type){

        if($type == 'past'){
            $vizits = Vizit::where('date', '<', Carbon::now()->subDay() )->orderBy('date', 'desc')->get();
        }else{
            $vizits = Vizit::where('date', '>=', Carbon::now()->subDay())->orderBy('date', 'desc')->get();
        }


        return view('admin.vizits.index', compact('vizits'));
    }
}
