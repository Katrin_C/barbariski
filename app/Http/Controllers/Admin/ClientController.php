<?php

<<<<<<< HEAD
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class clientController extends Controller
{
    //
=======
namespace App\Http\Controllers\Admin;

use App\Client;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ClientController extends AdminController
{
    public function index(){

        $users = Client::get();

        return view('admin.clients.index', compact('users'));
    }

    public function create(Request $request)
    {
        $manager =  User::where('id', Auth::user()->id)->first();
        return view('admin.clients.create', compact('manager'));
    }

    public function store(Request $request)
    {

        $validationRules = [
            'manager_id'    => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'dob' => 'required',
//            'first_vizit' => 'required',
//            'info' => 'required',
//            'is_garden_client' => 'required',
        ];
        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return Redirect::to('admin/clients/create')
                ->withInput()
                ->withErrors($validator);
        }

        // add
        $client = new Client();
        $client->manager_id = $request->manager_id;
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->second_name = $request->second_name;
        $client->gender = $request->gender;
        $client->dob = $request->dob;
//        $client->first_vizit = $request->first_vizit;
//        $client->info = $request->info;
//        $client->is_garden_client = $request->is_garden_client;
        $client->phone = $request->phone;
        $client->filia = $request->filia;
        $client->group = $request->group;
        $client->save();


//        // add to contact log
//        $contactLog = new ContactLog();
//        $contactLog->contact_id = $contact->id;
//        $contactLog->last_contact_at = $contact->last_contact_at;
//        $contactLog->last_contact_type = $contact->last_contact_type;
//        $contactLog->last_contact_details = $contact->last_contact_details;
//        $contactLog->save();


        Session::flash('success', 'Contact Added');
        return Redirect::to('admin/clients/' . $client->id . '/edit');
    }

    public function edit($id)
    {
        $client = Client::where('id', $id)->first();
        if(null === $client) {
            Session::flash('error', 'Not found client #' . $id);
            return Redirect::to('/clients');
        }
        return view('admin.clients.edit', compact('client'));
    }

    public function update($id, Request $request)
    {
        $client = Client::where('id', $id)->first();
        if(null === $client) {
            Session::flash('error', 'Not found client #' . $id);
            return Redirect::to('/clients');
        }

        $validationRules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'dob' => 'required',
        ];
        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return Redirect::to('admin/clients/'. $client->id . '/edit')
                ->withInput()
                ->withErrors($validator);
        }

        // update

        $client->manager_id = $request->manager_id;
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->second_name = $request->second_name;
        $client->gender = $request->gender;
        $client->dob = $request->dob;
//        $client->first_vizit = $request->first_vizit;
//        $client->info = $request->info;
//        $client->is_garden_client = $request->is_garden_client;
        $client->phone = $request->phone;
        $client->filia = $request->filia;
        $client->group = $request->group;
        $client->save();

        Session::flash('success', 'Updated Client #' . $client->id);

        return view('admin.clients.edit', compact('client'));
    }



>>>>>>> 675511f496d41257c8acb2eb3b3173648b4a0548
}
