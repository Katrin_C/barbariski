<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Filia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FiliaController extends AdminController
{
    public function index(){

        $filias = Filia::get();

        return view('admin.filias.index', compact('filias'));
    }
}
