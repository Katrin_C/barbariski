<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends AdminController
{
    public function index(){

//        $users = User::where('type', 'user')->get();
        if(in_array(Auth::user()->type, [User::TYPE_ADMIN])) {
            $users = User::get();

        }else{
            $users = User::where('type', 'user')->get();
        }


        return view('admin.users.index', compact('users'));
    }
}
