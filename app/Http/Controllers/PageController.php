<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function proposals()
    {
        return view('pages/proposals');
    }

    public function rules()
    {
        return view('pages/rules');
    }

    public function contact()
    {
        return view('pages/contact');
    }

    public function news()
    {
        return view('pages/news');
    }

    public function photos()
    {
        return view('pages/photos');
    }
    public function price()
    {
        return view('pages/price');
    }
    public function howitworks()
    {
        return view('pages/howitworks');
    }
}
