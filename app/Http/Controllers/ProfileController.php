<?php

namespace App\Http\Controllers;

use App\Child;
use App\User;
use App\Vizit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function showProfile($id){

        $user = User::where('id', $id)->first();

        if( Auth::user()->id == $id){
            return view('user/profile');
        }elseif(null === $user){
            Session::flash('error', 'Not found User #' . $id);
            return Redirect::to('home');
        }
        else{
            return redirect('home');
        }
    }

    public function update($id, Request $request){

        $user = User::where('id', $id)->first();

        if(null === $user) {
            Session::flash('error', 'Not found User #' . $id);
            return Redirect::to('home');
        }

        $validationRules = [
            'first_name'   => 'max:255',
            'last_name'    => 'max:255',
            'email'        => 'required|unique:users,email,' . $user->id,
            'bilding'    => 'numeric',
            'room'    => 'numeric',
            'entrance'    => 'numeric',

        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return Redirect::to('user/' . $user->id)
                ->withInput()
                ->withErrors($validator);
        }


        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->notes = $request->input('notes');
        $user->phone = $request->input('phone');
        $user->save();

        Session::flash('success', 'Updated User');

        return Redirect::to('user/' . $user->id);

    }

    public function addChild($id, Request $request){

        $validationRules = [
            'last_name' => 'required',
            'first_name' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            ];

        $messages = [
            'last_name.required'    => 'Фамилия ребенка должна быть указана',
            'first_name.required'    => 'Укажите имя ребенка',
            'gender.required' => 'Укажите пол ребенка',
            'dob.required'      => 'Поле Дата рождения обязательно к заполнению',

        ];


        $validator = Validator::make($request->all(), $validationRules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'NotValid',
                'error' => 'Not valid field(s)',
                'errorFields' => $validator->errors()->toArray(),
            ]);
        }


        if(Auth::user()->id == $id){

            $child = new Child();
            $child->last_name = $request->input('last_name');
            $child->first_name = $request->input('first_name');
            $child->gender = $request->input('gender');
            $child->parents_id = $id;

            $birth_date = $request->input('dob');
             $child->dob = Carbon::createFromFormat('d-m-Y H', $birth_date.'00');

            $child->info = $request->input('info');
            $child->ballans_hour = '2';
            $child->save();

            return response()->json([
                'status' => 'OK',
                'parents_id' => $id,
                'child' => $child,

            ]);
        }

    }

    public function createVisit()
    {

        if(Auth::check()){
            return view('user/createVisit');
        }
        return view('user/createFirstVisit');

    }

    public function postVisit(Request $request)
    {
        if(Auth::check()){

            $validationRules = [
                'last_name'         => 'required',
                'first_name'        => 'required',
                'phone'             => 'required',
                'child_id'          => 'required',
                'visit_date'        => 'required',
                'visit_time_from'   => 'required',
                'visit_time_to'     => 'required',
            ];
            $validator = Validator::make($request->all(), $validationRules);
            if ($validator->fails()) {
                return Redirect::to('createVisit')
                    ->withInput()
                    ->withErrors($validator);
            }

            $vizit = new Vizit();
            $vizit->parent_id = Auth::user()->id;
            $vizit->contact_last_name = $request->input('last_name');
            $vizit->contact_first_name = $request->input('first_name');
            $vizit->contact_phone = $request->input('phone');
            $vizit->child_id = $request->input('child_id');

            $date = $request->input('visit_date');
            $vizit->date = Carbon::createFromFormat('d-m-Y H', $date.'00');

            $vizit->visit_time_from = $request->input('visit_time_from');
            $vizit->visit_time_to = $request->input('visit_time_to');
            $vizit->notes = $request->input('notes');
            $vizit->save();

            Session::flash('success', 'Create vizit');

            return Redirect::to( 'user/'. Auth::user()->id);

        }else{
            $validationRules = [
                'email'        => 'required|unique:users,email',
            ];
            $messages = [
                'email.unique'    =>
                    'Пользователь с таким email уже зарегистрирован в системе. Попробуйте зайти в кабинет и оформить визит.
                        <a href=' . url('login') . '> Login </a>',
            ];
            $validator = Validator::make($request->all(), $validationRules, $messages);

            if ($validator->fails()) {
                return Redirect::to('createVisit')
                    ->withInput()
                    ->withErrors($validator);
            }

        }

    }
}
