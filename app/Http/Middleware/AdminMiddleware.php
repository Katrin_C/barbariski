<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use App\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || !in_array(Auth::user()->type, [User::TYPE_ADMIN, User::TYPE_MANAGER])) {
            Auth::logout();

            Session::flash('error', 'Allowed only for Administrator accounts');
            return Redirect::to('admin');
        }

        return $next($request);
    }
}