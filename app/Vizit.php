<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vizit extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    public function child()
    {
        return $this->hasOne(Child::class, 'id', 'child_id');
    }

    public function parent()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

}
