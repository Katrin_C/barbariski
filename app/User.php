<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const TYPE_ADMIN    = 'admin';
    const TYPE_MANAGER  = 'manager';
    const TYPE_CLIENT  = 'client';
    const TYPE_USER  = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'dob'
    ];

    public function vizits()
    {
        return $this->hasMany(Vizit::class, 'parent_id', 'id');
    }


}
